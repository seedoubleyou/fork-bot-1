from telegram import Update
from telegram.ext import ApplicationBuilder, CommandHandler, ContextTypes


async def hello(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await update.message.reply_text(f'Hello {update.effective_user.first_name}')


app = ApplicationBuilder().token("Put your own token here").build()

app.add_handler(CommandHandler("hello", hello))

app.run_polling()

def main():
    print ("This is the main function.")
    print ("Nothing to do for now, so I will exit.")
