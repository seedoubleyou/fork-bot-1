# bot1


## Table of Contents 

* [Name](#Name)
* [Description](#Description)
* [Badges](#Badges)
* [Visuals](#visuals)
* [Installation](#installation)
* [Usage](#usage)
* [Support](#Support)
* [Roadmap](#roadmap)
* [Contributors](#contributors)
* [License](#license)
* [Project Status](#project-status)


## Name
Whitaker Telegram Bot

## Description
The Whitaker telegram bot is a student project by the intermediate python G1 class that will fill the needs of the Jeff.pro telegram page not available in currently available bots.

## Badges
Coming soon

## Visuals
![image](beep_boop_beep1.png)

## Installation
Coming soon

## Usage
Coming soon


## Support
Coming soon


## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributors
### Team members
Aftab Shivdasani @generic47, 
Charmin Weis @Charminbw02,
Chris @seedoubleyou,
Connie Smith @Chargerdog,
Dan Schumacher @dansch2002,
David Garza @davidg25,
Dennis @TravelingTheUSA,
Diane Albers @elleneia,
Donna @dfdakota,
Emily Fraser @TNlass,
Glenna Ladner @Gladner,
Izabela Krupska @ikrupska,
Jay Conant @majorchaos,
Joe Freeze @j.freeze,
JP a.k.a. Ed Clark @edclark30,
MaryEllen Stevens @MaryEllen2,
Michael Macks McCosh @macksm3,
Paulina Piwowarek @2851778,
Pepe Lazarillo @pepelazarillo,
Shane Cooper @rippletide, 
Shawn Eckhardt @shawn.eck,
Susan Barnes @KY06SCB, 
Tim Mccallum @TXPatriot,
cheido @cheido,
susi crawford @susiQ369


## License
For open source projects, say how it is licensed.

## Project status
Just starting
